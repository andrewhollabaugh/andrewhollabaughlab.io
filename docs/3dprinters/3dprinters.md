# 3D Printers

![allprinters](allprinters.jpg)

##Tim's Printer (Summer 2013)
![timprinter](timprinter.jpg)

Back when I was in middle school, my dad bought my brother (Tim) a 3D printer that was a kit. This printer was the [RepRapPro Tricolour Mendel](http://reprap.org/wiki/RepRapPro_Tricolour). This printer was basically the "deluxe" version, because it has three extruders. This can be used to print in three different materials/colors in the same print. Also, all of the custom plastic parts on the printer were 3D printed, so they can easily be replaced. This printer was the start of many of my projects, including my quadcopters and solar car. Almost all of my projects have 3D printed parts in them. The printer is really only a modified version of the first consumer available 3D printer, created by the RepRap project. I wanted to help Tim build the printer, but he didn't want me to help for the most part. After he got it working, I learned how to use it and how it works. Tim made many modifications, which I sometimes helped with. 

##How a 3D Printer Works
This type of 3D printer uses [FFF](http://reprap.org/wiki/FFF) (Fused Filament Fabrication) to create objects. The plastic used for printing is 1.75mm diameter filament that comes in a spool. The type of plastic used is usually PLA or ABS. Printing with PLA is generally more reliable, while ABS can be used for high-temperature applications.

![benprinter2](benprinter2.jpg)

The filament is fed into the extruder drive, which regulates how much filament is being inserted. The filament goes down the bowden tube to the hot end, where it is squirted out a 0.5mm or 0.3mm nozzle. The hot end moves in the x and z directions, while the print bed moves in the y direction. The print bed is also heated so the object being printed sticks to it sufficiently. The object is printed in layers, usually 0.3 or 0.2mm. Our printers have a [raspberry pi](https://www.raspberrypi.org/) (cheap linux computer board) that runs a web server called Octoprint. This allows you to control the printer through your local network or the internet.

##Modeling Software
First, I used Google Sketchup to create designs. This program is easy to use and start with, but can get very annoying with complex designs. A while later, I started using OpenSCAD, a modeling software where the user writes code to generate the object. It took some time for me to be able to use it effectively, but it was an interesting experience. Now I usually use Onshape, a cloud-based CAD program similar to Autodesk. I also have some experience with Autodesk Inventor and Freecad. (Freecad is a free version of Autodesk) 

##Robostorm Club Printer (Fall 2014)
![clubprinter](clubprinter.jpg)
The 4-H robotics club that I am part of, Robostorm, bought the same RepRap printer for its members to build and use. The only difference between this printer and Tim's is the Robostorm printer is a Mono Mendel, so it only has one extruder instead of three. Since I had experience with 3D printing, I had a big part in building it. After it was built, the printer was passed around to many Robostorm members to use. The custom 3D printed parts on this printer were printed by Tim's printer to save the club some money.

Since this printer was built mainly by kids who didn't know what they were doing, it had many issues over the years. As long as the printer still worked for the most part, most of the issues were ignored. This printer was used at the 4H fair to print custom nametags for fairgoers. At a few times during the fair, it had problems and I was the one to fix them. The most severe of these problems was the control board getting fried. Tim had an extra one, so I had to replace it during the fair. Eventually (Summer 2016), the control board got fried again. We decided to fix all of the printer's issues at once and make some modifications, and I took lead of doing so. I suspected the reason why the board had failed was because of faulty wiring, so I decided to re-wire most of the printer. While doing this, I found the likely source of the problem, the heated bed wires were getting run over by the heated bed and their insulation was melted in some spots. This could have caused the wires to short, which would fry the control board. I also replaced the glass print surface with a PCB surface and installed a cable chain for the heated bed wires so they wouldn't get melted again. Another modifcation is the cork underneath the heated bed. The purpose of this is to insulate the bed from other parts so it heats up faster and stays heated for longer. Another modification is 3d printed fan covers on both of the fans. This may not sound like much, but when the printer is used by kids, you need to make sure they don't stick their fingers in the fan. After all this, I am now known as the 3d printer repair man.

##Ben's Printer (Summer 2016)
![benprinter](benprinter.jpg)
The founder of Robostorm (Ben) had received the same type of 3d printer. He built the frame and some other parts, but never finished it. He gave it to Tim and I to finish, since he will be leaving for college soon. Tim had other things to do, so I took charge in finishing it. Along with finishing building it, I made a few modifications from the start. This includes a PCB print surface and a cable chain for the heated bed wires. I also put another cooling fan on the back of the hot end, pointed at a downward angle. The purpose of this is to cool the object as it is printing. This is very helpful for prints that include bridging or other features that require fast cooling.
