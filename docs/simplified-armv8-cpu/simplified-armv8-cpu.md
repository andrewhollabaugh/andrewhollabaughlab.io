# Simplified ARMv8 CPU

Designed a 64-bit processor and implemented it in Verilog HDL. This project was part of a Computer Architecture class. The processor's architecture is a modified ARMv8 architecture, known as LEGv8. It implements 28 instructions, and includes a 64-bit timer peripheral.

More information can be found in the [datasheet](datasheet.pdf) I wrote.

