# Arduino Remote Controlled Car

![picture](arduinoremotecontrolledcar.jpg)

##Introduction
This was the first robotics project I completed. I took the frame and motors of an old toy RC car and put in my own electronics, using an Arduino. The car included a small DC motor and a solenoid to control steering.

##Original Electronics
The arduino I used was the Boarduino from Adafruit. This was useful for prototyping because I plugs directly into a breadboard, without jumper wires. I originally used a TV remote to control the car. This was a novel idea, but wasn't practical because I needed to point the remote directly at the car in order for it to receive the signal. Getting the TV remote to work involved using the serial monitor and the arduino printing the button codes that the remote sends it through an infrared reciever. I copied the codes for the buttons I wanted, and hard-coded them into my code. To control the motor and solenoid, I used the [SN754410](http://www.ti.com/lit/ds/symlink/sn754410.pdf) motor driver chip.

##Later Improvements
I later changed it to use a controller from an RC airplane. The receiver outputted PWM signals, so I used the Arduino to read the PWM. The Arduino board was changed to a Tennsy 2.0++ because I could process the PWM signals faster. I also created a circuit board for the car, instead of just using a breadboard.
