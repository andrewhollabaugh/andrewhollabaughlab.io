# Wii Classic Controller to USB Converter

![wiiclassicusb1](wiiclassicusb1.jpg)

This was a relatively simple project done to use a Wii Classic Controller as a USB gamepad. I wrote code on a Teensy 2.0++ to take inputs from the Wii Classic Controller and emulate a USB gamepad. The Wii Classic Controller uses standard I2C to communicate with a Wii Remote, so this signal can be interpreted by the Teensy. I used the WiiClassy Arduino library for this and the Teensy Joystick library for usb joystick output. I used the "Nunchuky 1.0" from Solarbotics to connect the Wii controller to a not solderless breadboard. I 3D printed a box for protection, which was designed in Onshape.

![wiiclassicusb2](wiiclassicusb2.jpg)
