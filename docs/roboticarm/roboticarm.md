# Robotic Arm

![picture1](roboticarm1.png)

## Introduction
This project is a robotic arm with 7 directions of freedom. All the plastic parts on the arm and the case for the circuit board were 3D-Printed on my 3D-Printer. I didn’t design the robotic arm itself, I found someone else’s design on the internet and built the arm according to that design. The arm uses 8 servo motors to move each segment of the arm.

## Electronics

I designed the circuit board and made it myself using the toner-tranfer/etching method. It uses an Arduino Mega, which is a programmable microcontroller, to control the arm. The circuit board plugs directly on top of the Arduino.

The Arduino microcontroller receives signals from the Wii Classic controller telling it what positions the joysticks are in and what buttons are currently pressed. Then the microcontroller interprets the signals and sends separate signals to it’s 8 servo motors, which each control a different movement on the arm.

![picture3](roboticarm3.png)
