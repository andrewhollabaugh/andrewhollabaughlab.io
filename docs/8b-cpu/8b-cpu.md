# 8 Bit CPU Design Implemented Using Discrete Logic ICs
This project involves the design of a custom 8-bit CPU and its implementation using discrete logic ICs (Integrated Circuits) on a Printed Circuit Board (PCB). Most of the ICs were are part of the 74xx logic series. Each logic IC represents a specific logic function, such as logic gates, multiplexers, and registers. The architecture is designed explicitly for implmentation using logic ICs, so many design decisions and optimizations were made explicity because of this type of implementation. Although the final PCB is quite large and difficult to use for any real application, the architecture was designed to be Turing Complete, and be usable enough for basic embedded applications, provided external peripherals are added based on the application.

Currently, the PCB is assembled though there are some issues that need to be resolved pertaining to the flash memory programmer. The CPU hasn't been tested yet as a result.

This project was started for fun, then used as a course project for an Advanced Computer Architecture elective class I took in college.

[Click here to read the full report](report.pdf)

The KiCAD EDA files, verilog model, flash programmer firmware and other design files can be found in the [GitLab repository](https://gitlab.com/andrewhollabaugh/8b-cpu/).

![pcb](pcb.png)

