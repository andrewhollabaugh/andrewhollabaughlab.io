# A Better NXT

![picture1](abetternxt1.jpg)

## Introduction
The LEGO Mindstorms NXT is a robotics kit used by many people to learn basic robotics. It is great for beginners, but the NXT brick, the programmable “brain” of the system, has many drawbacks for experienced roboticists. For example,  it only has 3 motor ports and 4 sensor ports, which hinders the amount of functions a robot can have.

What I have created is a replacement for the NXT brick, while still being compatible with non-LEGO technology. All the electronics fit into a red 3D Printed box with holes for mounting to LEGOs. It is currently being used on my LEGO battlebot for the competition at the 2015 4-H Fair.

The original NXT brick was simple, it had sensor inputs and motor outputs. But with my better version, much more can be achieved.

## Specifications
* 6 motor outputs: could be used for NXT motors, other LEGO motors, or any other 9v motor (<2A)
* 4 servo motor outputs: servo motors are an accurate, degree-based type of motor
* Maximum 10 sensor inputs: exact number depends types of sensors used
* R/C receiver: can use a 2.4GHz radio transmitter for remote controlling robots
* Serial and I2C communication: useful for communicating with other computers
* 12v compatible: all LEGO systems run a 9v, but this replacement can be plugged into a 12v battery instead of a 9v battery for use of 12v motors.

![picture2](abetternxt2.jpg)

## Electronics
---
The electronics in this module include the control board, motor controllers (MC), battery, and R/C receiver. I designed and made the control board myself, but the MCs and R/C receiver were bought off the internet.

## Control Board
The control board is the heart of the system, containing a programmable microcontroller called an Arduino. The specific Arduino board I am using is the Teensy 2.0++, made by PJRC. The Arduino is programmed using the C programming language. Programming the Arduino is essentially programming the entire module, since there are no other computers inside.

## Motor Controllers
The motor controller (MC) is another important component. The box has room for three MCs, but there are only two shown in the picture. Each MC can control two motors. The job of the MC is to provide the right amount of voltage to the motor with the right polarity at a high current, while taking a much lower current and lower voltage input. There are three signals that come from the Arduino and go each half of each MC. This includes two digital signals to indicate motor direction, and one PWM (Pulse Width Modulation) signal to indicate motor speed.

## R/C Receiver
The R/C receiver communicates over the 2.4GHz range to receive commands from an R/C transmitter. The signals that the receiver outputs to the Arduino are a PWM signals that can be measured using interrupts. The use of the radio transmitter is much nicer than the IR remotes or bluetooth communication that the NXT uses.
