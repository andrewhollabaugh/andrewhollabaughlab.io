# Simple Point-of-Sale System

![pos](pos.png)

This was a simple point-of-sale (POS) application I wrote for Robostorm, the Hunterdon County 4H Robotics Club. I used the Python programming language, and the Tkinter GUI library. It also interfaces with a cash drawer and a barcode scanner. The club used this application at the Hunterdon County 4H Fair to assist in selling 3D printed items as a fundraiser.

The list on the left of the interface contains the items in the current transaction. Items can be added by scanning items with the barcode scanner or manually entering them with the drop-down menu and 'Insert Item' button. The 'Apply Transaction' button is meant to be pressed when the customer pays, and will automatically open the cash drawer. The list on the right side shows how many of each item has been sold on the day specified in the above drop-down menu.

Interfacing with the barcode scanner scanner was simple. It acts as a keyboard, so the application has a normal text input bar for inputting barcodes.

The cash drawer opens with a 12VDC signal is applied to the solenoid contained in it. I used a MOSFET and an Arduino to control this. The POS application sends a character over serial to the Arduino, which then applies a positive voltage to the gate on the MOSFET, which allows 12V current from a power supply to activate the solenoid.

