# DC-DC Buck Converter
I designed and implemented a variable output, DC to DC buck converter with a feedback control loop. This device converts a higher input voltage into a lower output voltage at a high efficiency, and the exact output voltage can be specified. The converter accepts an input voltage of 7V, and the output voltage can be varied from 0 to about 6.8V. The classic buck converter circuit topology is used, which includes a single MOSFET, inductor, and capacitor.

An STM32 microcontroller is used to control the converter, which was programmed in the [Rust](https://www.rust-lang.org/) programming language. It outputs a PWM (Pulse Width Modulation) signal which is used to control the MOSFET, which acts as the switch. A PID (Proportional Integral Derivative) controller is implemeneted in software on the microcontroller to regulate the output voltage. The user sends commands over a serial port from a host computer in order to specify the output voltage.

The buck converter and control circuitry is implemented on a milled PCB, with a separate PCB for the configurable 5 or 10 ohm load. The converter was found to be at least 80% effiecent under most use cases, but effiencies are often 95% or higher for high output voltages.

This was a class project for a Power Electronics course I took Spring 2021, though I went far above and beyond the project requirements.

[Click here to read the full report](report.pdf)

The KiCAD EDA files and microcontroller firmware can be found in the [GitLab repository](https://gitlab.com/andrewhollabaugh/dcdc-buck/).

![picture](2.jpg)

