# Mazebot

![mazebot1](mazebot1.jpg)

## Introduction
The Mazebots are small remote-controlled robots. They were made for the Hunterdon County 4-H Robotics Club for the 2014, 2015, and 2016 4-H fairs. They would be dropped in a maze and the person controlling it would have to navigate it out of the maze.

## R/C Communication
The Mazebots are controlled using an R/C transmitter to communicate with the R/C receiver on the Mazebot using 2.4GHz technology. This type of communication is commonly used for R/C cars, boats, airplanes, helicopters, and multirotors.

## Construction
The Mazebot consists of custom 3D Printed parts that I designed in OpenSCAD. ([Thingiverse page](http://www.thingiverse.com/thing:1710287)) It is driven by two continuous rotation servo motors, which drive the two front wheels. This type of drive system is known as differential drive or tank drive. There is also sliding wheel on the back to keep the mazebot upright.

## Electrionics
The electronics of the Mazebots consist of the Arduino microcontroller, R/C receiver, battery pack (4 AAA batteries), and two servomotors. A custom built circuit board connects all of the elements. I designed the circuit board in Eagle, then used the etching method to make it. This board also includes a 5v regulator, since the Arduino can only handle 5v and the battery voltage is 6v. The Arduino is a programmable microcontroller, which is programmed using the C programming language. The Arduino board this uses is the Teensy 2.0.

![mazebot2](mazebot2.jpg)
