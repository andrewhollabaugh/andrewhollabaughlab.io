# Balsa Model Airplane

![picture](modelairplane.jpg)

## Introduction
This is a model airplane that I designed and built myself. It has a wingspan of 24 inches and is about 20 inches long. It has only been flown a few times, since it is extremely delicate and I am not a great pilot.

## Frame
The plane is made of a balsa wood frame structure with shrink plastic covering. I built the plane from scratch and it took several weeks to build. First, I came up with design plans on my own from other designs and tips found on the internet. Then I drew full scale plans on graph paper. Next I built the balsa structure right onto the plans using T-pins and Elmers wood glue. This was a very tedious process, taking much longer than I orignally thought. I ended up rebuilding certain parts because I just realized how weak they were. Some of the parts, including the wing and feusalage, have balsa walls on the outside for extra strength. After building the strcuture, I used heat-shrinking plastic covering to cover the structure.

## Electronics
The electronics include a 450mah 3 cell lithium-polymer battery, a small brushless motor, a 12A brushless speed controller, an OrangeRX 6ch reciever (I only used 4 channels) and four linear servos. All the electronics besides the motor and servos are housed inside the feusalage of the plane. The motor is mounted onto a 3D Printed firewall, which is hot-glued onto the feusalage. The servos are attatched using double-sided foam tape. They use bent paper clips and 3D Printed brackets to attatch to the control surfaces. The plane doesn’t include landing gear. My plan was to add this later, but it never happened.

## Flights
My first test flight was a complete failure, the directions of the aileron servos was reversed. This didn’t cause severe damage since the plane didn’t get far. I remember the next flight being somewhat successful, I was able to fly around without major failure. The plane went much faster than any plane I have flown before, and didn’t generate enough lift to fly without going very fast. The motor I used is also very powerful for this setup, I was able to go straight up without a problem! But this was short-lived because when I landed, I broke the feusalage and had to build another one. I was feeling very bold on my next flight; I tried to do a loop, which snapped the wing, making the plane plummet to the ground and break more parts! After this flight, I rebuilt what was broken and decided to built wing struts to keep the wing from snapping. The wing struts I built were 3D printed and flying with them proved they were way too heavy. I took them off and decided just to not do a loop so I wouldn’t need them. I recall that I flew one more time after that, and never flew it again because of how delicate it was.

## Conclusion
This was an important learning experience. A project that seemed easy and fun at first turned out to be a large undertaking. If I ever design or build another model airplane again, I will make it out of foam. For airplanes, foam is a material that is light and much stronger and easier to use than balsa and covering.

