# Equipment Lockout System
A system which only allows access to physical equipment in Rowan University's Engineering Department to those who have proper training. Users insert their Rowan ID card into a slot on the machine, the system authenticates, then the machine's power is enabled. The lockout system was proposed some of Rowan's faculty, and I took charge of the project as part of the A-Team (Apprentice Engineering Team).

A kiosk is mounted on each machine, which allows the user to interface with the system. Each kiosk is known as a 'lockout device', and communicates with a server with a 915MHz radio network. Gateways translate between the 915MHz network and WebSockets, which are used to communicate with the server. The server runs a web user interface, where administrators can control the status of each machine. The server determines who can use each machine based on the user's Rowan ID card number, and whether they have taken the required training for a given machine.

A fully-functional prototype of the system was created, though it has not been implemented on any machines (yet).

#### Git repositories
- [Device/Gateway microcontroller firmware](https://gitlab.com/A-Team-Rowan-University/lockout/device-firmware)
- [Server/Web UI](https://gitlab.com/A-Team-Rowan-University/web-development/resource-website/-/tree/263-lockout-device-initial-communication/resource-website/lockout)
- [Gateway Raspberry Pi Code](https://gitlab.com/A-Team-Rowan-University/lockout/gateway-rpi)

![lockout-block-detailed](lockout-block-detailed.png)

## Device Main Board
Each piece of equipment (milling machines, laser cutters, pick-and-place machines, etc) has its power controlled by a device. Each device contains an STM32 microcontroller and a 915MHz radio module, the SPIRIT1 from ST. Devices contain a MIFARE card reader, an LCD display, and a few other peripherals. When an ID card is scanned, the 16-byte ID is sent over the 915MHz radio to the server. If the Card ID correlates with someone who is authorized to use the machine, the server sends a message back to enable the machine.

![device](device-pcbnew.jpg)

The device main boards have been fabricated, and one is shown above. See the Appendix for schematics and PCB designs.

#### Microcontroller
The STM32G071 microcontroller (ARM Cortex M0+) was chosen, and is programmed in C++ using development tools including the [CLion IDE](https://www.jetbrains.com/clion/), [openocd](http://openocd.org/), and the [STLINKV3 debugger](https://www.st.com/en/development-tools/stlink-v3mods.html).

#### Radio Module
The [SPIRIT1](https://www.st.com/en/wireless-transceivers-mcus-and-modules/spirit1.html) radio tranciever from STMicroelectronics uses the ISM band (915MHz), and is not tied to a specific radio protocol. I essentially created by own protocol for this project, basing it off of the built-in packet handling engine of the SPIRIT1. A 1/4-wave dipole antenna is used on the devices. Each device communicates directly with a gateway, which relays messages to/from the server over WebSockets. More info on the communications protocol can be found [here](communications-protocol.pdf).

This 915MHz transciever was chosen over 2.4GHz transcievers due to the very high utilization of 2.4GHz frequencies on Rowan's campus, so the reliability would be questionable. 915MHz signals will also penetrate through materials more easier, so it is more likely that fewer Gateways will be needed.

#### ID Card Reader
The Rowan ID card reader is based on the MFRC522 RFID card reader IC. The Rowan ID cards are just regular Mifare RFID cards, and are read reliably using this method after the ID is inserted into a slot on the (not yet designed) enclosure. Generic breakout PCBs are used for the MFRC522 IC, since these are actually cheaper than buying the ICs themselves, and using a separate PCB gives extra flexibility for the orientation of the ID slot.

#### User Interface
A 20x2 character LCD display shows the status of the machine, and a buzzer will beep when the erroneous behavior occurs. User-facing buttons are present, which allow the user to ask for help, report misuse, or indicate the machine is broken.

#### What happens if the network isn't working?
If the server or gateway is down, RF signals are too weak, or software bugs exist, this could impede usage of the machines entirely if not dealt with properly. To combat this issue, a keyswitch is present on each device. Lab technicians who have access to the physical key can enable the machine no matter what. This even bypasses the device microcontroller, having a separate hardware enable to the power board. A potential future feature is to store the card ID numbers of lab technicians on the EEPROM chip on each device, so technicians can enable with their ID cards even if the network is down.

## Power Electronics

A separate power box is mounted on each machine, which contains the Power PCB. This PCB connects to the main board with a Cat6/RJ45 cable, and is designed to be generic so it can be used for any machine. It contains circuitry to drive relays to control the power to machines, many of which use 240VAC 3-phase. Different machines may use different relays, so this PCB was designed to drive any 12V or 24V relay.

The relays are controlled by a digital signal from the main board, which controls the [BTS50080](https://www.infineon.com/dgdl/Infineon-BTS50080-1TEA-DS-v01_00-EN.pdf?fileId=5546d4625a888733015aa435f2d2115b) smart high-side switch IC on the power interface board, which controls the relay coils. This board also accepts 12 or 24VDC from a wallwart, which powers this board and the main board via a DC-DC converter.

![power](power-pcbnew.jpg)

## Gateway
The gateway's job is to relay network transmissions between the 915MHz network (devices) and the server. It consists of a [Raspberry Pi](https://en.wikipedia.org/wiki/Raspberry_Pi) connected to a custom PCB containing an STM32 microcontroller and 915MHz radio. The Raspberry Pi is connected to Rowan's WiFi network, so it can set up WebSockets with the server.

The custom PCB uses the SPIRIT1 radio IC directly, and includes all the passive RF components required for it to function, following the reference design provided by ST. A PCB trace antenna is used, though connectors for other types of antennas are included if better range is needed. The schematic and PCB layout is shown in the Appendix.

![gateway](gateway.jpg)

The microcontroller and Raspberry Pi communicate using UART (serial). The serial protocol is essentially the same as the payload for the radio communication, but with some extra bytes added, including providing RSSI (Received Signal Strength Indicator) data from received 915MHz signals to the server.

The gateways use the same microcontroller as the devices, the STM32G071. The codebase is shared as well, using a directory structure that allows much of the code to be simultaneously used by both. Target-specific source code is kept to a minimum, and is kept in separate directories for each target. Target-generic code has access to the specific hardware details it needs through custom structs.

The Raspberry Pi runs a Python script that relays messages between the microcontroller over UART and the server, over WebSockets.

## Server and Web Interface
A web interface was developed using [Django](https://www.djangoproject.com/), a Python web framework. Lockout-specific functionality was added to an existing codebase to interface with existing training and safety testing systems.

Lab technicians can interact with the machines using the machines page of the web interface, shown below. The machine's status is shown, which includes whether it is enabled, if force disable mode is on (used to disable access to the machine if it is broken), and the communication status with the device. An events log is shown, which allows technicians to keep track of who uses the machine and when. Work entries can be created, which are used to keep track of repairs. The machine can also be enabled or disabled remotely.

![machine-page](machine-page.png)

Below is the device page, which shows all the details of all packets sent to or from a given device.

![device-page](device-page.png)

## Flowchart
Describes behavoir of the lockout device.

![lockout-flowchart](lockout-flowchart.png)

## Appendix: Schematics/PCB Layout
[KiCAD](https://www.kicad.org/), a free and open-source EDA suite, was used to create the schematics and PCB layouts.

Right click and select "Open image in new tab" to see a larger, more readable version.

![device-sch](device-sch.png)
Device main board schematic

![device-pcb](device-pcb.png)
Device main board PCB layout

![power-sch](power-sch.png)
Power board schematic

![power-pcb2](power-pcb2.png)
Power board PCB layout

![gateway-sch](gateway-sch.png)
Gateway schematic

![gateway-pcb](gateway-pcb.png)
Gateway PCB layout



