# Design, Layout, and Simulation of a 4-Bit Parity Generator
The transistor-level design of a 4-bit parity generator circuit, and its implementation using VLSI methodology. This was part of a VLSI design class taken at Rowan University. [Read the full report](report.pdf)

