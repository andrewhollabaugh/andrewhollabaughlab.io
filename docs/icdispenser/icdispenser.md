# Integrated Circuit Dispenser

![main](main.jpg)

## Introduction
The IC (Integrated Circuit) Dispenser is a machine that stores and dispenses ICs from their tube packaging. ICs frequently come packaged in IC tubes, which are long pastic tubes specifically designed to fit the ICs. This project uses a chain system to store and index many IC tubes. This version is a prototype, and can store a maximum of 25 tubes, though only 6 are shown in the images. It can also dispense individual ICs from the tubes upon request. It currently supports two types of ICs, DIP and TO-220, though more similar types will be added in the future.

On an abstract level the IC Dispenser contains two mechanisms, the selector and the dispenser. Each is moved using a stepper motor. The selector moves the chain the tubes are attached to, allowing for selection of the tube from which to dispense. The dispenser consists of a toothed belt, which is driven into the selected tube and pushes out the specified number of ICs.

The mechanical design work was done in [Onshape](https://www.onshape.com/), an online CAD software. The frame was constructed from 20x20mm aluminum extrusion. All custom plastic parts are 3D printed.

The low-level functionality is controlled by an Atmel ATMEGA328 microcontroller. This AVR-based microcontroller, programmed in C (no arduino library) uses UART (serial) to communicate with a master device. This master device, which will likely be a Raspberry Pi or similar device, runs a GUI (Graphical User Interface). The GUI, written in Python, allows users to enter ICs to dispense and to debug the machine using lower-level functions.

#### Links
- [Full CAD Model (Onshape)](https://cad.onshape.com/documents/9faa237c6a2fd460a8ef0aaa/w/f7aad9b9dd57eac17d439bba/e/934dcfe066eacae6e3050998)
- [Embedded C Program (Github)](https://github.com/andrewhollabaugh/icDispenser-embedded)
- [Graphical Interface Code (Github)](https://github.com/andrewhollabaugh/icDispenser-gui)

## Purpose
This is a project that I have been working on since November of 2018, where it started out as an early dispenser prototype. I declared the project 'done for now' in January 2020, with a fully-functional prototype.

I work in the Electrical & Computer Engineering Resource Center at Rowan University. This is where electronic components and tools are stored for the department. The original goal was to implement a large version in the Resource Center for day-to-day use. A staff member would use the machine to dispense the ICs and for organization. 

I eventually decided to stop work on the project after "finishing" the prototype due to the realization that this machine is not a practical solution that I believe would be a good idea to fully implement. The concept and usability of the device has inherent problems such as training staff to use it properly and maintainence. The machine would be used relatively infrequently, so I believe it would only cause more problems than it would solve. Anyway, it was an amazing learning experience that I am glad I accomplished.

## Tube Selector
![front](front.jpg)

The selector mechanism moves using a stepper motor, which drives a chain system. Two #25 chains, one on the front and one on the back, are connected with a shaft. 3D printed sprockets, 1 motor driven and 3 idlers, contrain the chain to a rigid shape. On the outside of the chain, 3D printed chain clips attach to the chain. These attach to tube mounts, which use a clamping system to have a good grip on the tube. To change the tubes in the IC Dispenser, the screw on the front tube mounts can be loosened, then the tube can be slid in or out.

![dispenser](dispenser.jpg)

The chain clips on the rear chain system have 'flags' on them. These flags pass through an IR detector, which is used to keep track of where the selector is in relation to the tubes. The IR detector is the black object with two nubs, mounted on the dispsenser assembly as shown in the above picture.

Each tube is assigned an index. When the selector is moving, it keeps track of which index it is at by counting pulses from the IR detector. The IR detector also is used align the tubes with the dispenser mechanism. One edge of the IR sensor is aligned with an edge of the flags, so the selector can stop in exactly the right place so the dispenser is lined up with the tube.

The tube at index 0 is special; its chain clip has a wider flag on it. This is used for selector homing. When the IC Dispenser is first turned on, it doesn't know where it is. It must home itself before it can dispense. It moves the selector until it finds the wider flag, which it knows is home, or index 0. When it finds the first edge of the flag, it starts counting steps moved by the stepper motor. When it gets to the other edge of the flag it stops counting. If the number of steps is greater than a threshold, it found home (wider flag), otherwise it keeps moving.

## Dispenser
![belt-drive](belt-drive.png)

A CAD rendering of the dispenser assembly is shown above, with the cover transparent. The toothed belt, used to push ICs out of the tubes, resides in the channel inside the dispenser assembly. It is driven by the sprocket connected to the stepper motor. After the belt is extended out the front to dispense some ICs, it comes back and homes itself. It uses a limit switch to home in exactly the same location every time.

A tape measure (the light green object) is used to store the excess belt. This is very important since the belt is quite long, longer than the entire length of the IC Dispenser itself. I took apart a tape measure, and removed the measurement tape from the spring. I then attached the end of the belt to the spring. The spring gives a retraction force that ensures the excess belt is stored nicely inside the tape measure housing.

![sensor](sensor.jpg)

When the belt goes through the tube to push out the ICs, how does it know when to stop? It uses an IR detector, similar to the one used by the selector. This detector, however, has an independent IR emitter and sensor, spaced much farther apart. When an IC is pushed out of a tube, it falls between the emitter and sensor. This blocks the IR light from the emitter from reaching the sensor, causing a digital pulse on the sensor's output signal. When a pulse occurs, the dispenser knows to stop, increment a counter for how many ICs have been dispensed, then continue. It is important that the space between the emitter and sensor is large, since tubes have varying lengths.

## PCB/Electronics
![schematic](schematic.png)

The electronics are implemented on a 2-layer custom PCB (Printed Circuit Board). The board was designed in [KiCAD](https://kicad-pcb.org/) and sent to JLC to be manufactured. The surface mount components were soldered using Rowan University's pick-and-place machine and reflow oven. The ATMEGA328 chip on the PCB is surface mount, and does not come with the correct fuse configuration or bootloader from the factory. I used an ArduinoISP programmer to do this; now the board can be programmed over UART (serial) directly.

![board-design](board-design.png)

## Graphical Interface
![gui](gui.png)

The Graphical User Interface (GUI) was written in Python. It uses the Tkinter library for the window and graphical widgets.

Uses select ICs from the list on the left, which adds them to the 'ICs to Dispense' list. The ICs in this list can be dispensed using the dispense button. The 'Advanced' section has debug features that will likely be put in a separate menu later. The log on the right displays past activity, and is also written to a log file.

