![picture](quadcopter400.jpg)

## Introduction
This quadcopter is similar to my other 250 quadcopter, but this one is much bigger. It is 400 class, meaning that there is 400mm of space between diagonal rotors.

Just like the smaller copter, this one also has a frame that I designed and 3D printed.

## Power System
It uses the Multistar 2212 920kv motors. Of course, these motors are much bigger and more powerful than the motors used for my smaller quadcopter, although the smaller motors do spin much faster. I also use a 3-cell battery for this copter, either a  3300mah, 2700mah, or 2200mah battery.

The motors came bundled with propellors, which are 9.5in props, the same ones used on the popular Phantom 2 quadcopters.

It uses the same type of control board used by my 250 quad, with MultiWii installed.
