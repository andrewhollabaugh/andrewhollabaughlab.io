# 3D Printed Quadcopter

![V2.2](V2.2.jpg)

##Overview
This is a 250 class quadcopter that I designed and built using 3d printed parts. From the start I have always been interested in making design modifications to make this design the best it can be. This quadcopter was initially designed in OpenSCAD, then moved to Onshape. I wanted the challenge of creating a quadcopter with plastic, a material weaker than the commonly used carbon fiber. I thought I could use design ingenuity to overcome this and still create a quality design. At this point, I beleive I have succeeded.

This design can be downloaded at my [Thingiverse page](http://www.thingiverse.com/thing:1554635).

##How a Quadcopter Works
A quadcopter (also known as a “drone”) is a helicopter that uses four propellers to generate lift. Using computer algorithms, it adjusts the speeds of each of the motors to tilt itself and move horizontally. Quadcopters are included in a group of flying machines known as multirotors, which also includes tricopters, hexacopters, and octocopters. In general, the more rotors, the more stable a copter is, but four is effective enough for its price. My quadcopter is is a class 250. This means that the distance between diagonal rotors is 250mm.

Multirotors use brushless outrunner motors. Like most brushless motors, they require 3-phase AC current to run. ESCs (Electronic Speed Controllers) convert the DC battery power into the correct signal for the motors. The ESCs receive PWM signals from the control board to tell them how fast to spin the motors. The control board uses inputs from the RC receiver to calculate what speed each of the motors need to go in order for the quadcopter to move correctly. The power source for multirotors is a lithium polymer battery, usually three or four cells.

Multirotors can be flown line-of-sight, but it is more fun to fly FPV. FPV stands for First Person View. This way, the quadcopter is flown by looking at a live video feed from a camera on the quadcopter. This way, you can go much faster and much farther than you could with line-of-sight. This technology works by using a camera and a video transmitter on the quadcopter, and a video receiver and screen on the ground. Even though this technology is relatively new, the video signal is still analog since it can be processed faster. Many FPV quadcopters, including mine, have two cameras. One is a dedicated FPV camera (bottom), and one is an HD video recording camera (top).

##List of Electronics
* Control board - Flip32+ with Cleanflight
* ESCs - DYS 20A with BLHeli
* Motors - DYS 1806-2300kv
* Battery - Turnigy nano-tech 1500mah 35C
* Propellors - 5x3, 5x3 tri, 5x4, 5x4 tri
* RC Transmitter/Receiver - Turnigy 9X
* Low battery alarm
* Voltage Regulator (5v)
* Power distribution - Brass strips
* FPV Camera - RunCam PZ0420H-L28 FPV Camera NTSC (2.8mm lens)
* Recording Camera - Mobius Actioncam
* FPV Transmitter - Boscam TS832
* FPV Receiver - Boscam RC832
* FPV Antenna - Aomway cloverleaf

##My Current Design (V2.5)
---

![topview](topview.jpg)

My design is based around two plates and two side panels that form a "box," with the arms mounted in the corners of the box. Inside the box is the delicate electronics and wiring, including the control board, RC receiver, ESCs, voltage regulator, and power distribution. The design premise here is to have a sturdy frame "core" that will not warp or bend. It is also extremely unlikely to break.

![inside](inside.jpg)

![arm](arm.jpg)

The arms were deliberatly made as short as possible. The purpose of this is so more torque is required for them to break. Also, the arms will fold inwards. This does help for storage, but the main purpose of this is to increase the time of impact when in a crash. When a crash happens, if an arm is hit hard from the front it will fold inwards and not break. This feature has been in my design since the very beginning, and it has saved countless arms. The landing gear is meant to act as a fuse. In a crash. the landing gear will always be the first thing to break. This is not a bad thing, since it breaking reduces the chance of other parts breaking. Also, the landing gear is designed to be very quick and easy to print and replace, with it only being held on with zip ties. The landing gear is still resilient to withstand light crashes though. The battery can be mounted on the top or bottom, but I prefer the bottom. This lowers the center of gravity, which makes the quadcopter more stable.

![front](front.jpg)

The mount for the dedicated FPV camera is held on by nylon screws. The purpose of this is that the nylon screws will break instead of the camera. The mount for the Mobius camera (for video recording) uses the same design premise. It is attatched with a zip tie, and wil slide or rotate in a crash instead of breaking it. This same mounting style is used for the video transmitter. The frame uses M3 hardware for everything. (except for motor mounts) Metal screws are used for the arm mounts. The arms need to be very tightly held on so they absorb more energy when folding in a crash.

##Earlier Designs
---

###V2.1
![V2.1](V2.1.jpg)
####Changes
* Landing gear: now 3d printed, held on with zip ties

###V2.0
![V2.0](V2.0.jpg)
####Changes
* Redesigned frame to be much more compact and robust
* Frame is closed; RC receiver, control board, power distribution, other things are ineccesible
* Battery mount: changed to top
* Mobius mount: changed to bottom
* FPV trasmitter mount: changed to side

###V1.0
![V1.0](V1.0.jpg)
####Features
* Original design
* Design based off carbon fiber frames
* Frame is open; all electronics are somewhat exposed
* Folding arms
* Landing gear: brass tubes
* Battery mounting: bottom
* Mobius mount: front
* FPV transmitter mount: top

##Earlier Quadcopters

###Quadcopter 400
![picture](quadcopter400.jpg)

This quadcopter is similar to my other 250 quadcopter, but this one is much bigger. It is 400 class, meaning that there is 400mm of space between diagonal rotors.

Just like the smaller copter, this one also has a frame that I designed and 3D printed.

#### Power System
It uses the Multistar 2212 920kv motors. Of course, these motors are much bigger and more powerful than the motors used for my smaller quadcopter, although the smaller motors do spin much faster. I also use a 3-cell battery for this copter, either a  3300mah, 2700mah, or 2200mah battery.

The motors came bundled with propellors, which are 9.5in props, the same ones used on the popular Phantom 2 quadcopters.

It uses the same type of control board used by my 250 quad, with MultiWii installed.

###Quadcopter 350
This was the first quadcopter I made. I made the mistake of designing and 3D Printing my own frame when I had no experience with multirotors.

#### Power System
I used the Emax 1806 2280kv motors. For being a 350 class quadcopter (350mm between diagonal rotors) these motors are very underpowered. To get a little more lift out of these motors, I used 6 inch propellers instead of 5 inch props, even though they aren’t meant to be used with these motors.

#### Frame
The frame itself was very inefficient and very heavy, even for its size. I used metal screws for everything, used a heavy terminal strip for power distribution.

#### Conclusion
This quadcopter flew a few times, but it was so underpowered it was barely flyable. When I switched to a different radio transmitter, I forgot to calibrate the ESCs. Since this radio outputted much higher values on each channel, the motors spun much faster than they are meant to. At first I thought this was a good thing because I got more lift, but it burned two of the motors out.

In the end I realized how stupid and heavy my design was, and completely scrapped it. Since I only had two motors left, I bought four more powerful motors and new ESCs to go along with them for my new design. This new design became my Quadcopter 400.

