## Introduction
This was the first quadcopter I made. I made the mistake of designing and 3D Printing my own frame when I had no experience with multirotors.

## Power System
I made the mistake of using the Emax 1806 2280kv motors rebranded as Turnigy off of Hobbyking. For being a 350 class quadcopter (350mm between diagonal rotors) these motors are very underpowered. To get a little more lift out of these motors, I used 6 inch propellers instead of 5 inch props, even though they aren’t meant to be used with these motors.

## Frame
The frame itself was very inefficient and very heavy, even for its size. I used metal screws for everything, used a heavy terminal strip for power distribution, and thought that propeller guards were essential. (Now I know that they break extremely easily and don’t really make it much safer)

## Conclusion
This quad flew a few times, but it was so underpowered it was barely flyable. When I switched to a different radio transmitter, I forgot to calibrate the ESCs. Since this radio outputted much higher values on each channel, the motors spun much faster than they are meant to. At first I thought this was a good thing because I got more lift, but it burned two of the motors out.

In the end I realized how stupid and heavy my design was, and completely scrapped it. Since I only had two motors left, I bought four more powerful motors and new ESCs to go along with them for my new design. This new design became my Quadcopter 400.
