# Andrew Hollabaugh
---

On this site you will find documentation of my engineering projects. My more recent projects include:

- [Equipment Lockout System](https://andrewhollabaugh.gitlab.io/lockout/lockout/) (June 2020 - December 2021)
- [Hardware Accelerator for Differential Equations](https://andrewhollabaugh.gitlab.io/diffeq-accel/diffeq-accel/) (June 2021 - Present)
- [4-Bit Parity Generator VLSI](https://andrewhollabaugh.gitlab.io/parity/parity) (December 2021)
- [DC-DC Buck Converter](https://andrewhollabaugh.gitlab.io/dcdc-buck/dcdc-buck/) (April 2021)
- [8-bit Processor Implemented using Logic ICs](https://andrewhollabaugh.gitlab.io/8b-cpu/8b-cpu/) (March 2020 - December 2021)
- [Integrated Circuit Dispenser](https://andrewhollabaugh.gitlab.io/icdispenser/icdispenser/) (December 2018 - January 2020)

