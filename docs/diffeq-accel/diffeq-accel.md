# Hardware Accelerator for Solving Differential Equations
A hardware accelerator module to solve systems of differential equations using numerical methods. The Lotka-Volterra family of differential equations was used as a starting point, which are used in the field of computational biology. The accelerator was developed using the [Chisel](https://www.chisel-lang.org/) language and the [Chipyard](https://github.com/ucb-bar/chipyard) environment, which includes RISC-V CPUs and other hardware accelerators written in Chisel.

The purpose of a hardware accelerator is to increase performance and decrease energy consumption for a specific type of computation, in this case solving a specific family of differential equations. The equations are implemented directly in hardware, creating an accelerator module that interfaces with a general-purpose RISC-V CPU.

A paper titled ["An Open-Source Co-Processor for Solving Lotka-Volterra Equations"](paper.pdf) was presented at ISCAS 2022 (IEEE International Symposium of Circuits and Systems). For this paper, a hardware accelerator was developed specifically for the Lotka-Volterra system, and its performance and hardware costs were characterized.

Development continues on creating a generation tool for these co-processors. The system of equations is taken as an input, and will generate a co-processor design which is optimized for that system. The real purpose is to allow automatic generation of co-processors for highly complex systems involving hundreds of equations.

#### Software Repositories
- [diffeq-accel](https://gitlab.com/andrewhollabaugh/diffeq-accel/)
- [diffeq-accel-generator](https://gitlab.com/andrewhollabaugh/diffeq-accel-generator/)

