# FTC Team 12601

![ftc](ftc.jpg)

## Introduction
In my senior year of high school I started an FTC (FIRST Tech Challenge) team, Team 12601. This team was part of Robostorm, the Hunterdon County 4H Robotics Club. I did a lot of the initial research for starting the team, and proposed the idea to the club. We competed during the 2017-2018 school year in the NJ FTC league. I was on the team for one year. I am currently still involved with the team, as a mentor. The rest of this page will describe the 2017-2018 year and my contributions to the team.

In FTC, teams create a robot to compete in that year's challenge. The challenge varies from year to year. That year's challenge was called 'Relic Recovery,' and involed stacking foam cubes and placing the 'relic' (a plastic idol-shaped object) outside of the field. The robots are a maximum of 18 inches in all dimensions.

## The 2017-2018 Robot
As shown in the picture above, this robot was fully custom machined. This was done by one of the other team members (and his father). The robot's drive train uses mecanum wheels. This type of wheel has 45 degree rollers, allowing for sideways movement. The robot uses a double-gripper system to grab two foam cubes, one at a time, and transport them to the stacking area. The main arm includes a retractable extension that allows for grabbing and placing the relics.

The robot performed exceptionally well. Many other robots looked as if they were constructed with twigs and programmed with slightly modified example code. We did well enough to make it to the World Championship in Detriot, where we did quite well.

## Software
My primary contribution was on the software side. The programming for FTC robots in done in Java, using their libraries. These libraries obscure most of the details, allowing for a high-level and easy-to-use interface. This is great for teams with no programming experience, but I found this quite annoying.

Anyway, I used an object-oriented programming style. I created objects for the mechanisms of the robot, such as the grabber arm and drive base. The 30-second autonomous mode of the robot was quite effective, achieving all of the usual tasks during this time period. This was done using simple dead-reckoning using drive motor encoder values.

The program ended up quite large and complex compared to most other FTC teams. This is due to the complexity of the arm mechanism. I used somewhat of a state-machine to ensure the mechanisms were in the right places before an action could occur.

My code can be found on [here](https://github.com/Robostorm/Robot-Code-2017-2018) on Github.

