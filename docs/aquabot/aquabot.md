# Aquabot

![aquabot](aquabot.jpg)

## Introduction
The Aquabot is a remote-controlled, bottled water dispensing robot. This has been a project of Robostorm, the Hunterdon County 4H Robotics club, for several years. The purpose it to use it as a fundraiser during the annual 4H Fair. We drive it around the fairgrounds selling bottled water for $1 to fairgoers.

I have been involved in the Aquabot project since its start, though I was the most involved and actively in charge during 2018, so I will highlight my experiences from that year.

## More About the Aquabot
The drive base, adapted from a powered wheelchair, consists of two powered wheels and two caster wheels. The two wheel motors were powered by 12V coming from the 12V sealed lead-acid battery. The remote control, a re-purposed remote control airplane controller, sends values to the receiver on the Aquabot, drive and turn. An Arduino microcontroller converts these two signals into motor power values fed to the motor controller. The Arduino also implements a few safety features as well. A bumper was installed on the front for safety purposes. When hit, a limit switch is pressed, triggering the safety relay which shuts off power to the motors.

The bottle dispensing system is entirely 3D printed. On top of the drive base is a cooler, which houses the bottle storage and dispensing system. It consists of a shelf and several guides for bottles, as well as the dispenser itself. The dispenser is controlled by a servo, which rotates back and forth to dispense each bottle. The cooler can store up to 10 bottles at once. A dollar bill acceptor is present to collect the money. When a bill is inserted, it then automatically dispenses a bottle.

The dispsening system is controlled by a Teensy++ 2.0 microcontroller (programmed in Arduino). This controller also controls three seprate RGB LED strips, driven by MOSFETs. The electronics and money boxes are constructed from acrylic with 3D printed hinges and latches. There is also a keypad and LCD display for keeping track of sales how many bottles are left in the Aquabot.

## 2018 Updates
The main goal of this year was to update the Aquabot's electronics and re-make some mechanical components to keep it reliable for a long time. I lead a team of several members the 4H club to accomplish this.

Since I was working with a bunch of kids, I acted as the person in charge. I scheduled meetings, kept records, determined what needed to get done when, and made sure the Aquabot was ready for the 4H Fair. Most of the actual work on the Aquabot was done by the kids, with me acting as a mentor.

The most daunting task was creating new control electronics for the bottle dispensing system. The old electronics consisted of two separate PCBs (Printed Circuit Boards), and lots of hacked-together wiring. The goal was to unify the PCBs, and create a separate power distribution PCB. I designed both PCBs in KiCAD, and they were made using the toner-transfer/etching method.

Another large task was re-making the acrylic boxes to house the money and electronics. These boxes were put together with hot glue, but this failed pretty quickly. We used acrylic glue this time so hopefully they won't fall apart again.

Since the electronics for the dispenser needed to be redone, pretty much all the dispenser wiring needed to be redone as well. We added real connectors to a lot of the wires and heat-shrink tubing (we used hot glue instead of heat shrink in the past and it was terrible!). 

